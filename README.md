PNGPal
======

Little command line tool that replaces colours in images according to a specified palette.

```sh
pngpal by bzt Copyright (C) 2021 MIT license
 https://gitlab.com/bztsrc/pngpal

./pngpal [-m <num>] [-p <palfile>] [-c] [-t] [-g] <input image> <output png>

 -m <num>       set the maximum number of colours
 -p <palfile>   load palette (aco, ase, jasc-pal, gpl, paint.net, any image)
 -c             use CIE76 method instead of sRGB distance
 -t             make the output a truecolor image
 -g             generate palette ramps
 <input image>  input image (png, jpg, gif, tga, bmp, pnm, psd)
 <output png>   output image, always png (truecolor or indexed if possible)
```

### Installation

Just download, no dependencies, no install, portable executables.

- [pngpal](https://gitlab.com/bztsrc/pngpal/raw/main/pngpal) (Linux, 322k)
- [pngpal.exe](https://gitlab.com/bztsrc/pngpal/raw/main/pngpal.exe) (Windows, 354k)

### Compilation

Just run `make` in the src directory. No configure, no dependencies, suckless as it should be.

Specifying the Palette
----------------------

You can read the palette from various formats. If the `-m` option is given, then it will read
that many colours (up to 256).

- GIMP Palette files (.gpl)
- Adobe Photoshop Color File (.aco)
- Adobe Swatch Exchange files (.ase)
- Microsoft Palette files, JASC-PAL (.pal)
- Paint.NET Palette files (.hex, .txt)
- Graphic Interchange Format (.gif)
- Portable Network Graphics (.png, 8-bit indexed, with PLTE chunk or truecolor with less than 256 different colours)

In theory multiple colourspaces supported in palette files (grayscale, HSV, CMYK, LAB, RGB), but only RGB was tested.
With images for the best results, you should use an indexed image (gif or quantized png), or a true-color image that
has no more than 256 different colors in it. Dimensions and number of pixels doesn't matter, only how many different
pixels there are.

Alpha values never read from the palettes, instead the original image's alpha channel is preserved.

Matching Colours
----------------

For the colour conversion, by default sRGB distance is used. It is fast and provides very good results most of
the time. However in some edge cases it might choose bad colours, so you can use the `-c` flag to use the CIE76
method (which converts both the palette and the image to CIELAB colourspace and then calculates distances there.)

Input Image
-----------

Input can be in any format that [stb_image](http://nothings.org/stb) understands:

- Portable Network Graphics (.png, 8-bit, 16-bit, grayscale, truecolor, indexed etc.)
- Photoshop Document (.psd, limited features, not all files supported)
- Joint Photographic Experts Group's (.jpg .jpeg, not all variants supported)
- Graphic Interchange Format (.gif, animations supported too)
- Targa Graphics (.tga)
- Windows Bitmap (.bmp, non-rle compressed only)

For animated gifs, the frames are extracted vertically (first frame on the top, last frame at the bottom).

Output Image
------------

The converted image is saved as a PNG with alpha channel. No .so nor .dll needed, libpng included. If possible, then the
output is going to be indexed (8 bit per pixel plus 24+8 bit palette, much smaller file size), unless `-t` given, in which
case output is always saved as a 32 bit per pixel image, even if it contains less than 256 different colours.

If `-g` given, then the output won't be a converted image, rather it will contain the palette ramps of the input image,
vertically sorted by color (hue) and horizontally by decreasing luminance (not perfect, but good enough).

License
-------

Licensed under the terms of MIT license.


Cheers,

bzt
